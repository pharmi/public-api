# This script validates JWT tokens against a public ES512 key in the same way as the 
# token is validated on the public API backend

import jwt
import sys

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

if len(sys.argv) != 3:
    print('usage: python validate_token.py <public key file> <encoded token>')
    exit()

with open(sys.argv[1], 'rb') as pkfile:   
    public_key = serialization.load_pem_public_key(pkfile.read(), default_backend())
    print(public_key)
    token = sys.argv[2]

    t = jwt.decode(token, public_key, algorithms=['ES512'], options={"verify_exp": False})
    print(t)

