# Pharmi Public API Documentation

This repository contains the API documentation of the Pharmi Public API. Aside of the documentation, we provide example scripts for the JWT authentication mechanism of the Pharmi Public API.

## Getting started

### Required tools
In order to run all the scripts, you will need to have the following tools installed:

- [`openssl`](https://www.openssl.org/source/)
- [`node`](https://nodejs.dev/en/download/)
- [`python` and `pip`](https://www.python.org/downloads/) 

Some of these tools may be already installed on your system.

### Installing dependencies
For both the node and python scripts some dependencies need to be installed. 

- For node: use `npm install` or `yarn install`

## Creating a key pair

Use the following command to create a public and private key pair:

```sh
./e512-keypair.sh
```

The key files will be saved to your working directory as `ecdsa-p521-public.pem` and `ecdsa-p521-public.pem`.

To use the API, the public key needs to be sent to Pharmi. Together with your private key (which should not leave your organization) and your subject id (which Pharmi will provide to you), you will be able to create JWT access tokens that are used to authenticate to the public API.

## Generating a token
Use the following command to generate a token:
```sh
node create-token.mjs <private_key> <subject_id> [lifetime] [patient_identifier]
```

Where
- `private_key` is the private key file name.
- `subject_id` is your organization's subject id.
- `lifetime` string (e.g. `10m`, `1h`) is the max expiration of the JWT token. The default lifetime is 10 minutes. Note that the Pharmi production environment will not accept a token with a lifetime longer than 15 minutes.
- `patient_identifier` is the identifier of the patient, which is needed to access the web view components. This should be left out if the JWT will be used in REST API calls.

## Validating a token
You can validate your token against your public key by running

```sh
python3 validate_token.py <public_key> <token>
```
Where
- `public_key` is the public key file name
- `token` is the token string

## Trying the token
The easiest way to try whether your token was generated successfully and will be accepted by Pharmi is to try it out! You can use the token in our Postman collection, or run the following `curl` request:

```
curl --request GET 'https://api.pharmi.info/v1/pharmacy' \
--header 'Authorization: Bearer <token>'
```

If successful, this request will return a response with status `200` and a JSON response with content similar to:

```json
{
    "data": {
        "id": 7,
        "name": "Apotheek Pharmi"
    }
}
```

## See also
- [API documentation](api-doc.md)
- [OpenAPI Specification](https://app.swaggerhub.com/apis/PharmiBV/Pharmi-Public-API)
- [Pharmi API postman collection](https://pharmi1.sharepoint.com/:u:/s/Pharmi/EcaOkvlCAJ1Noi1l1VBnI3EBgoDquR1nNXAgHXRlZc1y3A?e=Nvlovb)
