# Pharmi Public API Documentation

[[_TOC_]]

## Introduction
Pharmi's public REST API is divided into two sections: the Patient Management API and the Patient Information API. The Patient Management API serves to create patients in the organization, link patients to treatments, and retrieve a patient’s QR code required to activate the MedicijnWijs app. The Patient Information API serves to retrieve patient specific treatment information including the information library and the timeline. This document describes the models and workflows for each of these APIs. 
 
Next to the REST API, there is the Medicijnwijs Web View functionality. This enables customers to integrate patient functionality into their application using a web-based version of the MedicijnWijs application. 

### Principles

### Base URL

The pharmi API is accessible through the following base url: 
 
`https://api.pharmi.info/v1/` 

### Versioning
We strive to provide a stable API and have no plans to introduce a new version of the API in the short or mid-term. Having said that, change is the only constant in software development so in the long term, a new major version of the API will likely be introduced at some point. When that happens, we will support the previous version of the API next to the new version for a considerable time. 
 
We do allow non-breaking changes to be introduced to the API under the current version number.  
Non-breaking changes to the API may be made under the /v1/ path. These are changes that do not break the existing contract of the API specification. 
- The following changes are considered non-breaking: 
- The addition of endpoints to the API specification 
- The addition of fields to request bodies and responses 
- Making required fields in request bodies or query strings optional 
- The addition of query parameters or header fields 
- Fixing unintended behaviour (bugs) 
 
The following changes are considered breaking and can only be introduced in a new major version of the API.  
- Renaming or removing endpoints 
- Renaming or removing fields in request bodies and responses 
- Making optional fields in request bodies required 
- Adding required query parameters or header fields 
- Renaming or removing query parameters or header fields 
- Changes in HTTP response codes 
- Changes in the behavior of endpoints 

### Authentication
Authentication is done with JWT tokens. 
- JWTs are implemented using an asynchronous algorithm, where the client shares the public key with Pharmi, while managing the private key themselves. 
- The client is responsible for creating JWT tokens for authentication. Pharmi validates the authentication against the public key. 
- Only the `ES512` algorithm is accepted. 
- JWT tokens are added as a Bearer token to the `AUTHORIZATION` header: `Authorization: Bearer <token>`. 
- All JWT must have an expiration time (exp) and an issued at time (iat). The time difference between the exp and iat is currently not enforced. We suggest to use a short expiration time in the order of minutes to ensure safety and prevent  . The IAT should always be in the past. 
- The JWT token must contain the id of the pharmacy as the subject in the sub field.  
- The JTI (JWT-id) should be filled in with a unique uuid for each token. This is currently not enforced. 

#### Generating an ES512 Key Pair

The `openssl` tool can be used to create an ES512 key pair used to sign and validate jwt tokens.

```sh
#private key 
openssl ecparam -genkey -name secp521r1 -noout -out ecdsa-p521-private.pem 
# public key 
openssl ec -in ecdsa-p521-private.pem -pubout -out ecdsa-p521-public.pem 
``` 

A shell script to generate a valid key pair can be found here: https://gitlab.com/pharmi/public-api/-/snippets/2341234

The public key must be shared with Pharmi to enable your organization for use of the public API. The private key will be used by your organization to sign the token. 
 
The private key must be kept secret and should not be communicated outside of your organization. 

#### Creating the token payload and signing a token 
The JWT tokens should be generated server-side by the client. JWT libraries are universally supported. An example Node JS script that generates a valid token based on a private key can be found here: https://gitlab.com/pharmi/public-api/-/snippets/2341234 

#### Example JWT token 

The following gives an example of a valid JWT token payload:  
 
```json
{ 
  "sub": 12345, 
  "iat": 1516239022, 
  "exp": 1516239082, 
  "jti": "76632bbc-d1bd-4af2-a2ae-db761e54decc" 
}
```

## Patient Management API
The Patient Management API serves to create patients in the organization, link patients to treatments, and retrieve a patient’s QR code required to activate the MedicijnWijs app 

### Model Description

#### Patient 
A Patient describes a patient’s identity. A Patient has the following fields: 
- a unique identifier within the scope of the customer (i.e., the patient’s identifier in the AIS system) 
- A unique id within the database 
- An email address (optional) 
- a first name (optional) 
- a last name (optional), 

In case the MedicijnWijs app is used, a QR code can be obtained from the API which is required to activate MedicijnWijs with the patients personal profile. 
 
#### Treatment 
A Treatment describes the identifying information for a treatment. A Treatment has the following fields: 
- a unique ID within the database 
- The treatment name
- an abbreviated name 
- an identifier. Currently this is the ZI number of the medication.  

A Treatment may involve medication, in which case the treatment name is typically the name of the medication. 
 
#### PatientTreatment 
A PatientTreatment is a link between a Patient and a Treatment (many-to-many) based on the foreign keys of each Patient and Treatment. A PatientTreatment has:

 - a unique ID
 - `patient` (or, in the `patient/creat_and_link_treatment` enpoint, `identifier`: a Patient identifier
 - `treatment_identifier`: a Treatment identifier. This can be the external identifier (for BASE treatments, this is the ZI number). Cannot be sent at the same time as `treatment_id`
 - `treatment_id`: the internal treatment id. Cannot be sent at the same time as `treatment_identifier`
 - `first_usage_date`: a date specifying when the treatment should begin, i.e. the date that the patient should receive the first guidance for the treatment in the Pharmi app. 
 - `confirmed`: a boolean value whether the treatment is confirmed. Note: unconfirmed treatments will not show moments in the timeline.

### Workflows

#### Pharmacy information
| Step | API Call | Notes |
| ---  | ---      | ---   |
| Retrieve information of the Pharmacy | `GET /pharmacy` | Returns the name and the id of the pharmacy you are authenticated for. Useful for debugging purposes. |

#### Creating a Patient

| Step | API Call | Notes |
| ---  | ---      | ---   |
| Create a patient | `POST /patient` | Refer to the patient model for required and optional fields. The patient can later be queried by supplying the unique patient id.  |
| Create a patient and link treatments | `POST /patient/create_and_link_treatment` | Create a patient if the identifier does not yet exist and link the treatment if it has not been linked already. It is a useful endpoint when all that is required is a patient with a treatment link, and no other bookkeeping is necessary.<p/>If the patient and the treatment links already exist, this endpoint does nothing and returns a 200 OK status.<p/>The request body takes an `identifier` to uniquely identify the patient within a customer's organization, and a `treatment_identifier` to identify the treatment (for BASE treatments, this is the ZI number). If the treatment start date should be different than today, it can be provided in the request body as `treatment_first_usage_date`. |

#### Viewing a patient’s details 

| Step | API Call | Notes |
| ---  | ---      | ---   |
| View a Patient | `GET /patient/:identifier` | The patient identifier is the unique patient identifier within the organization |
| Get QR code for the Patient | `GET /patient/:id/qr` |  

#### Updating a patient’s details 

| Step | API Call | Notes |
| ---  | ---      | ---   |
| Update a patient | `PUT /patient/:identifier` | The patient identifier is the unique patient identifier within the organization. | The QR code can be scanned by the patient in the Pharmi app to log in to their account. | 

#### Deleting a patient 

| Step | API Call | Notes |
| ---  | ---      | ---   |
| View a Patient | `DELETE /patient/:identifier` | The patient identifier is the unique patient identifier within the organization |

#### Linking a patient to a treatment 

| Step | API Call | Notes |
| ---  | ---      | ---   |
| Search available Treatments | `GET /treatment` | You can filter treatments e.g. by their ZI number, by using `?zi_number=0123456789` Treatments returned are treatments available to the organization. |
| Link patient to a treatment | `POST /patient_treatment` | Refer to the PatientTreatment model for required and optional fields. | 

#### Removing a treatment from a patient

| Step | API Call | Notes |
| ---  | ---      | ---   |
| Remove a Treatment from a patient’s profile | `DELETE /patient_treatment/:id` | The id is the internal id of the patient treatment object. This is returned in the `POST /patient_treatment` response. It can also be obtained using the `GET /patient/:identifier/details` endpoint. | 

#### Updating a treatment for a patient 

| Step | API Call | Notes |
| ---  | ---      | ---   |
| Update a treatment for a patient | `PUT /patient_treatment/:id` | The main use case for this is to change the start date of the treatment. The id is the internal id of the patient treatment object. This is returned in the `POST /patient_treatment` response. It can also be obtained using the `GET /patient/:identifier/details` endpoint |

## Patient Information API

The Patient Information API serves to retrieve patient specific treatment information including the information library and the timeline.  
 
### Model description 
#### Patient 
A Patient has the same identifying information for a Patient as a Patient in the Patient Management API, but in addition, it has a list of all the PatientTreatments this Patient is involved in (and hence, it contains all the Treatments linked to the Patient). 
 
#### Treatment 
A Treatment has the same identifying information for a treatment as a Treatment in the Patient Management API. 
 
#### PatientTreatment 
A PatientTreatment is a link between a Patient and a Treatment (many-to-many). A PatientTreatment has the same information as a PatientTreatment in the Patient Management API, except rather than a foreign key for a Treatment, it has an actual Treatment. 
 
#### Moment 
A Moment is a piece of information intended to be displayed to a patient at a specific time, as part of a treatment. A Moment has a title, content, and optionally, an image, video, and/or InformationItem specified by an InformationItem foreign key. Moments can also have one or more Questions

#### Question
An interactive question a patient can respond to, grouped together by Sections within a Moment.

A question object contains the following fields:
- `id`: the question id. Needed for sending in responses
- `title`: the question title
- `question`: the text of the question in markdown format
- `type`: The kind of question to be rendered. See table below for examples.
- `linear_scale_type`: If a question is of type `linear_scale`, this contains the type of linear scale, otherwise `null`.
- `choices`: If the question is of type `multiple_choice`, this array contains the choiche options in (id, value) tuples
- `ordering`: The place in the order in which the question should appear in te moment. The order is a sequential integer numbering and starts with `0`.
- `section`: Section information in which the question should be displayed.

The following question types are returned by the API and should be redered similar to the example provided.

| question type     | Description | Notes | Example Rendering |
| -------------     | ------------| ------| ----------------- |
| `open`           | an open ended question that accepts any textual input | There is currently no input size communicated through the API | ![Open ended question](img/q-open-long.png)
| `numeric_input`   | question that accepts numeric input only | |  ![numeric input question](img/q-numeric.png)
| `boolean`         | A yes/no question | | ![boolean question](img/q-boolean.png)
| `linear_scale` (5 points)          | A linear scale for rating  | should be rendered when `linear_scale_type` is `text-5` | ![5-point linear scale question](img/q-likert-5.png)
| `linear_scale` (7 points)          | A linear scale for rating | should be rendered when `linear_scale_type` is `text-7` | ![7-point linear scale question](img/q-likert-7.png)
| `linear_scale`    | A linear scale to communicate wellbeing | should be rendered when `linear_scale_type` is `smileys` | ![linear scale question with smileys](img/q-progress.png) |
| `date`            | A question for date input with a date picker | | ![date question](img/q-date.png)
| `multiple_choice` | A multiple choice question | the choices to render are sent in the `question_choices` field | ![multipli choice question](img/q-multiple-choice.png)
| `progress`    | A progress question, rendered as a 5-point smiley likert | | ![progress meter question](img/q-progress.png)

#### PatientResponse
A patient's response to a Question. The request body has the following fields:
- `question_id`: the internal identifier of the question to answer
- `response`: the stringified answer. E.g. if the answer is to a numeric question, the answer should still be encoded as a string
- `choice_responses` (optional): an array of the selected choice identifiers in case the question is multiple choice.
 
#### InformationItem 
An InformationItem is a piece of information that can be reused across moments. An InformationItem has a title, and optionally, content, an image, video, and/or child InformationItems. 
 
#### ClinicalPicture 
A treatment contains one or more clinical pictures, which are top-level objects of the treatment information library. A clinical picture contains one or more InformationItems 
 
#### Phase 
A Phase serves to organize Moments. As such, a Phase is a collection of Moments. A Phase has a title, and it has an ordering to indicate its place within a Timeline relative to other Phases. 
 
#### Timeline 
A Timeline contains a Treatment and a collection of Phases. This links the Phases to the Treatment for which they are intended. As such, the Timeline is the total collection of all pieces of information that will be displayed to a patient. 

### Workflows 
For these workflows, use the token with the pharmacy ID in the token and specify the patient identifier in the endpoints by prefacing them with /patient/:id/. The patient identifier is your internal identifier you specified when creating the patient.

#### Displaying the timeline of a patient

| Step | API Call | Notes |
| ---  | ---      | ---   |
| Get the Timeline for a Patient for a specific day | `GET patient/:identifier/timeline/:datestring` | Returns all the moments on a timeline for one patient, ordered by treatment and phase. <p/> The datestring parameter takes is in the ISO-8601 date format (`YYYY-MM-DD`). <p/> Note: unconfirmed treatments will not show moments in the timeline. |
| Get the image in a Moment or Information Item | `GET patient/:identifier/library_image/:imageid` | Any image can be obtained through this endpoint by passing its identifier |
| *DEPRECATED* Get the image in a Moment | `GET patient/:identifier/image/:momentid` | An image associated to a moment can be obtained through this endpoint |

#### Displaying the information library of a treatment 

| Step | API Call | Notes |
| ---  | ---      | ---   |
| Get details (including Treatments) of a Patient | `GET patient/:identifier/details` | The treatments inside the endpoint contains the IDs of the head (top-level) information items. |
| Get the information of a treatment | `GET patient/:identifier/treatment/:treatment_identifier` | The treatment identifier is the external identifier (for BASE treatments, this is the ZI number). |
| Get the information of a treatment by internal id | `GET patient/:identifier/treatment_id/:treatment_id` | The treatment id is the internal treatment identifier, not the treatment identifier string. |

#### Sending a patient Response to a Question
| Step | API Call | Notes |
| ---  | ---      | ---   |
| Answering a Question | `POST patient/:identifier/response` | Send in the response according to the `PatientResponse` format  |

## Web View
The web view makes it possible to embed the medication library of a patient as a web page in a third party application. The wev view endpoints return an html page that can be loaded as a stand-alone web page or can be embedded in an iframe. 
 
The base url for the webview is: `https://webview.pharmi.info/` 
 
Like the REST api, authentication of the web view occurs with JWT. The same JWT generation process can be used for API authentication, but the patient identifier must be added to the JWT payload with the key `patient_identifier`. This way the library view will be scoped to the patient information and cannot be used to obtain information from another patient. 
 
The token can be added to the request in two ways: 
through the `Authentication` header with the Bearer prefix, i.e. `Bearer <token>`
through url parameters, i.e. `?token=<token>`
 
### Example web view JWT payload

```json
{ 
  "sub": 12345, 
  "patient_identifier": "patientZero", 
  "iat": 1516239022, 
  "exp": 1516239082, 
  "jti": "76632bbc-d1bd-4af2-a2ae-db761e54decc" 
} 
```
### Web view endpoints 
| Step | API Call | Notes |
| ---  | ---      | ---   |
| Show the information library of a treatment. | `GET information/treatment/:treatment_identifier/` | Show the information library of a specific treatment. The `treatment_identifier` is optional in the event the patient only has one treatment linked to their profile. |
| Show the timeline of a patient | `GET /timeline` | Shows the timeline of a patient, starting on the current date |
