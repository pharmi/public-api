// This is a node script to generate a JWT token for the public pharmi API. 
// The jsonwebtoken dependency should be installed by adding it to the nearest 
// package.json file or by globally installing the dependency.

import fs from "fs";
import crypto from "crypto";
import jwt from "jsonwebtoken";


function createToken() {
    if (process.argv.length < 4 || process.argv.length > 6) {
        console.error("usage: node create-token.mjs <private key file> <subject id> [expires in] [patient-identifier]")
        return;
    }

    const privateKeyFile = process.argv[2];
    const subject = process.argv[3];
    const expiresIn = process.argv[4] ?? "10m";
    const patientIdentifier = process.argv[5] ?? null;

    try {
        const privateKey = fs.readFileSync(privateKeyFile, "utf-8");
        const payload = { sub: subject, jti: crypto.randomUUID() }
        if (patientIdentifier) {
            payload['patient_identifier'] = patientIdentifier;
        }
        const token = jwt.sign(
            payload,
            privateKey,
            { algorithm: 'ES512', expiresIn: expiresIn }
        );
        console.log(token);
    } catch (err) {
        console.error(err);
    }
}

createToken();